#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include <time.h>
#include <semaphore.h>

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"


enum direction {UP, DOWN, RIGHT, LEFT};

typedef struct nodeR{
    
    bool entry;
    bool exitMaze;
    bool wall;
    char displayValue;
    bool up;
    bool down;
    bool left;
    bool right;
} node_r;


typedef struct linkedA{
    struct nodeA * answer;
    struct linked_a * next;
} linked_a;

typedef struct nodeA{
    char direction;
    int steps;
    struct nodeA * next;
} node_a;

typedef struct {
    int x;
    int y;
    int sizeX;
    int sizeY;
    enum direction dir;
    node_a  * answer;
} threadData;

typedef struct {
    int sizeX;
    int sizeY;
} printData;

int running_threads = 0;

char exitChar = '/';
char entryChar = 'e';
char wallChar = '*';

linked_a * answers;

int t = 0;
pthread_t tid[400];
pthread_t tidPrint;


node_r * maze[200];
int sizeX, sizeY;

sem_t g_Sem;
volatile bool exitFlag = false;

void buildMaze(char * fileName, node_r * maze[]){
    
    FILE* file = fopen(fileName, "r");
    fscanf(file, "%d %d", &sizeX, &sizeY);
    char line[256];
    int x = 0;
    int y;
    fgets(line, sizeof(line), file);
    while (fgets(line, sizeof(line), file)) {
        y = 0;
        while (y < sizeY){
            printf("x %d y %d val %c \n", x,y,line[y]);
            maze[x*sizeY + y] = malloc(sizeof(node_r));
            maze[x*sizeY + y]->displayValue = line[y];
            maze[x*sizeY + y]->up = false;
            maze[x*sizeY + y]->down = false;
            maze[x*sizeY + y]->right = false;
            maze[x*sizeY + y]->left = false;
            if (line[y] == exitChar){
                maze[x*sizeY + y]->exitMaze = true;
            }else if (line[y] == wallChar){
                maze[x*sizeY + y]->wall = true;
            }else{
                maze[x*sizeY + y]->wall = false;
            }
            y ++;
        }
        x ++;
    }   
    printf("=================\n");
    fclose(file);
}



void forcePrintMaze(){
    int sizeX = sizeX;
    int sizeY = sizeY;
    printf(KNRM "======================\n");
    for (int x = 0; x < sizeX; x ++){
        for (int y = 0; y < sizeY; y ++){
            if (maze[x*sizeY + y]->displayValue == 'u'){
                printf(KRED "%c ", maze[x*sizeY + y]->displayValue);
            }else if (maze[x*sizeY + y]->displayValue == 'd'){
                printf(KGRN "%c ", maze[x*sizeY + y]->displayValue);
            }else if (maze[x*sizeY + y]->displayValue == 'l'){
                printf(KYEL "%c ", maze[x*sizeY + y]->displayValue);
            }else if (maze[x*sizeY + y]->displayValue == 'r'){
                printf(KBLU "%c ", maze[x*sizeY + y]->displayValue);
            }else{
                printf(KNRM "%c ", maze[x*sizeY + y]->displayValue);
            }
        }
        printf(KNRM "\n");
    }
    printf("======================\n");
}

void* printMaze(void * args){
    while (!exitFlag){
        sem_wait(&g_Sem);
    }
    exitFlag = false;

    threadData * currArgs = args;
    int sizeX = currArgs->sizeX;
    int sizeY = currArgs->sizeY;
    printf(KNRM "======================\n");
    for (int x = 0; x < sizeX; x ++){
        for (int y = 0; y < sizeY; y ++){
            if (maze[x*sizeY + y]->displayValue == 'u'){
                printf(KRED "%c ", maze[x*sizeY + y]->displayValue);
            }else if (maze[x*sizeY + y]->displayValue == 'd'){
                printf(KGRN "%c ", maze[x*sizeY + y]->displayValue);
            }else if (maze[x*sizeY + y]->displayValue == 'l'){
                printf(KYEL "%c ", maze[x*sizeY + y]->displayValue);
            }else if (maze[x*sizeY + y]->displayValue == 'r'){
                printf(KBLU "%c ", maze[x*sizeY + y]->displayValue);
            }else{
                printf(KNRM "%c ", maze[x*sizeY + y]->displayValue);
            }
        }
        printf(KNRM "\n");
    }
    printf("======================\n");
    exitFlag = true;
    sem_post(&g_Sem);
    sleep(1);
    printMaze(args);
}


void printAnswer(node_a * winner){
    node_a * current = winner;
    printf("Route : ");
    while(current != NULL){
        if (current->direction != ' '){
            printf("%c", current->direction);
            if (current->next != NULL){
                printf(",");
            }    
        }
        if (current->next == NULL){
            printf(" Total Steps : %d", current->steps);
        }
        current = current->next;
    }
    printf("\n");
}

void copyContent(node_a * a1, node_a * a2){    
    node_a * current = a1;
    node_a * current2 = a2;
    while (current !=NULL){
        current2->next = malloc(sizeof(node_a));
        current2->next->direction = current->direction;
        current2->next->steps = current->steps;
        current2->next->next = NULL;

        current2 = current2->next;
        current = current->next;
    }
}



void* findPath(void * args){
    while (!exitFlag){
        sem_wait(&g_Sem);
    }
    exitFlag = false;
    threadData * currArgs = args;
    int pos = currArgs  ->x * currArgs->sizeY + currArgs->y;
    bool didSomething = false;
    bool more = false;
    
    if (currArgs->dir == LEFT){
        node_a * current = currArgs->answer;
        while (current->next !=NULL){
            current = current->next;
        }
        current->next = malloc(sizeof(node_a));
        current->next->direction = 'l';
        current->next->steps = current->steps + 1;
        current->next->next = NULL;

        maze[pos]->displayValue = 'l';
        maze[pos]->left = true;
    }else if (currArgs->dir == RIGHT){

        node_a * current = currArgs->answer;
        while (current->next !=NULL){
            current = current->next;
        }
        current->next = malloc(sizeof(node_a));
        current->next->direction = 'r';
        current->next->steps = current->steps + 1;
        current->next->next = NULL;

        maze[pos]->displayValue = 'r';
        maze[pos]->right = true;
    }else if (currArgs->dir == UP){

        node_a * current = currArgs->answer;
        while (current->next !=NULL){
            current = current->next;
        }
        current->next = malloc(sizeof(node_a));
        current->next->direction = 'u';
        current->next->steps = current->steps + 1;
        current->next->next = NULL;

        maze[pos]->up = true;
        maze[pos]->displayValue = 'u';
    }else if (currArgs->dir == DOWN){

        node_a * current = currArgs->answer;
        while (current->next !=NULL){
            current = current->next;
        }
        current->next = malloc(sizeof(node_a));
        current->next->direction = 'd';
        current->next->steps = current->steps + 1;
        current->next->next = NULL;

        maze[pos]->down = true;
        maze[pos]->displayValue = 'd';
    }

    if (maze[pos]->exitMaze){                   //WINNER
        sleep(1);
        char word[256];
        maze[pos]->displayValue = exitChar;
        printf("Succesful \n");
        printAnswer(currArgs->answer);
        printf("Enter word...\n");
        scanf("%s" , word);

        exitFlag = true;
        sem_post(&g_Sem);
        running_threads --;
        return 0;
    }


    if (currArgs->y + 1 < currArgs->sizeY && !maze[pos + 1]->wall && !maze[pos + 1]->right && currArgs->dir != LEFT){     //RIGHT
        didSomething = true;
        threadData * newArgs = malloc(sizeof(threadData));
        newArgs->dir = RIGHT;
        newArgs->x = currArgs->x;
        newArgs->y = currArgs->y + 1;
        newArgs->sizeX = currArgs->sizeX;
        newArgs->sizeY = currArgs->sizeY;
        newArgs->answer = malloc(sizeof(node_a));
        newArgs->answer->next = NULL;
        newArgs->answer->direction = ' ';
        newArgs->answer->steps = 0;
        copyContent(currArgs->answer,newArgs->answer);
        
        exitFlag = true;
        if (currArgs->dir == RIGHT){
            sleep(2);
            more = true;
            findPath(newArgs);
        }
        else{
            sleep(1);
            t ++;
            running_threads ++;
            pthread_create(&(tid[t]), NULL, findPath, newArgs);
        }
        sem_post(&g_Sem);
        while (!exitFlag){
            sem_wait(&g_Sem);
        }
        exitFlag = false;
    }
    if (currArgs->y - 1 >= 0 && !maze[pos - 1]->wall && !maze[pos - 1]->left && currArgs->dir != RIGHT){     //LEFT
        didSomething = true;
        threadData * newArgs = malloc(sizeof(threadData));
        newArgs->dir = LEFT;
        newArgs->x = currArgs->x;
        newArgs->y = currArgs->y - 1;
        newArgs->sizeX = currArgs->sizeX;
        newArgs->sizeY = currArgs->sizeY;
        newArgs->answer = malloc(sizeof(node_a));
        newArgs->answer->next = NULL;
        newArgs->answer->direction = ' ';
        newArgs->answer->steps = 0;
        copyContent(currArgs->answer,newArgs->answer);

        exitFlag = true;
        if (currArgs->dir == LEFT){
            more = true;
            sleep(2);
            findPath(newArgs);
        }
        else{
            sleep(1);
            t ++;
            running_threads ++;
            pthread_create(&(tid[t]), NULL, findPath, newArgs);
        }
        
        sem_post(&g_Sem);
        while (!exitFlag){
            sem_wait(&g_Sem);
        }
        exitFlag = false;
    }
    if (currArgs->x + 1 < currArgs->sizeX && !maze[pos + currArgs->sizeY]->wall && 
            !maze[pos + currArgs->sizeY]->down && currArgs->dir != UP){     //DOWN
        didSomething = true;
        threadData * newArgs = malloc(sizeof(threadData));
        newArgs->dir = DOWN;
        newArgs->x = currArgs->x + 1;
        newArgs->y = currArgs->y;
        newArgs->sizeX = currArgs->sizeX;
        newArgs->sizeY = currArgs->sizeY;
        newArgs->answer = malloc(sizeof(node_a));
        newArgs->answer->next = NULL;
        newArgs->answer->direction = ' ';
        newArgs->answer->steps = 0;
        copyContent(currArgs->answer,newArgs->answer);

        exitFlag = true;
        if (currArgs->dir == DOWN){
            more = true;
            sleep(2);
            findPath(newArgs);
        }
        else{
            sleep(1);
            t ++;
            running_threads ++;
            pthread_create(&(tid[t]), NULL, findPath, newArgs);
        }

        sem_post(&g_Sem);
        while (!exitFlag){
            sem_wait(&g_Sem);
        }
        exitFlag = false;
    }
    if (currArgs->x - 1 >= 0 && !maze[pos - currArgs->sizeY]->wall && 
            !maze[pos - currArgs->sizeY ]->up && currArgs->dir != DOWN ){     //UP
        didSomething = true;
        threadData * newArgs = malloc(sizeof(threadData));
        newArgs->dir = UP;
        newArgs->x = currArgs->x - 1;
        newArgs->y = currArgs->y;
        newArgs->sizeX = currArgs->sizeX;
        newArgs->sizeY = currArgs->sizeY;
        newArgs->answer = malloc(sizeof(node_a));
        newArgs->answer->next = NULL;
        newArgs->answer->direction = ' ';
        newArgs->answer->steps = 0;
        copyContent(currArgs->answer,newArgs->answer);

        exitFlag = true;
        if (currArgs->dir == UP){
            more = true;
            sleep(2);
            findPath(newArgs);
        }
        else{
            sleep(1);
            t ++;
            running_threads ++;
            pthread_create(&(tid[t]), NULL, findPath, newArgs);
        }

        sem_post(&g_Sem);
        while (!exitFlag){
            sem_wait(&g_Sem);
        }
        exitFlag = false;
    }
    if (!didSomething){
        sleep(1);
        printf("Unsuccessful \n");
        printAnswer(currArgs->answer);
    }
    if (!more){
        running_threads --;
    }

    exitFlag = true;
    sem_post(&g_Sem);
}



int main()
{
    
    char *fileName;
    printf ("File Name: ");
    scanf("%s",fileName);

    buildMaze(fileName,maze);
    sem_init(&g_Sem, 0, 0);
    
    threadData * args = malloc(sizeof(threadData));
    args->x = 0;
    args->y = 0;
    args->sizeX = sizeX;
    args->sizeY = sizeY;
    args->answer = malloc(sizeof(node_a));
    args->dir = DOWN;

    answers = malloc(sizeof(node_a));
    answers->next = NULL;

    node_a * first = malloc(sizeof(node_a));
    first->next = NULL;
    first->direction = ' ';
    first->steps = 0;
    args->answer = first;

    running_threads ++;

    exitFlag = true;
    pthread_create(&(tidPrint), NULL, printMaze, args);
    pthread_create(&(tid[t]), NULL, findPath, args);

    while (true)
    {
        sleep(5);
    }

    return 0;
}
